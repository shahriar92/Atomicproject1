<?php

namespace App\BITM\SEIP107897\Hobby;

class Hobby {
    
    //public $id = "";
    public $title = "";
    //public $created = "";
    ///public $modified = "";
    // public $created_by = "";
    // public $modified_by = "";
    // public $deleted_at = ""; //soft delete
    
    public function __construct($title = false){
        $this->title = $title;
    }
    
    public function store(Model $model){
       
        $result = "I am storing data for ".$model->title;
        
        if($result){
            return "Mobile title is added successfully.";
        }else{
            return "There is an error while saving data. Please try again later.";
        }
    }
}
