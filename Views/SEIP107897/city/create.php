<!DOCTYPE html>
<html>
    <head>
        <title>Select city</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="store.php" method="post">
            <fieldset>
                <legend>Select City</legend>
                
                <div>
                    <label>Select city from the dropdown menu</label>
                    <select name="city">
                        <option>Dhaka</option>
                        <option>Chittagaong</option>
                        <option>Rajshahi</option>
                        <option>Sylet</option>
                        <option>Jessore</option>
                        <option>Barishal</option>
                        <option>Rangpur</option>
                        <option>Khulna</option>
                    </select>
                </div>
                <div>
                    <label>Enter city code</label>
                    <input placeholder="city code" 
                           type="number" 
                           name="code"
                           required="required"
                      
                           />
                </div>
                <button  type="submit">Save</button>
                <button  type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                <input type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>


