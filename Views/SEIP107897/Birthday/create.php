<!DOCTYPE html>
<html>
    <head>
        <title>Birthday</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="store.php" method="post">
            <fieldset>
                <legend>Add Birthday</legend>
                
                <div>
                    <label for="title" >Enter your Name</label>
                    <input autofocus="autofocus" 
                           id="title" 
                           placeholder="Enter your name" 
                           type="text" 
                           name="title"
                           tabindex="1"
                           required="required"
                           size="30"
                           />
                </div>
                <div>
                    <label>Enter birthday</label>
                    <input placeholder="Enter your birthday" 
                           type="date" 
                           name="day"
                           required="required"
                      
                           />
                </div>
                <button  type="submit">Save</button>
                <button  type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                <input type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>


