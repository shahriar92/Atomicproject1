<?php
   // ini_set('display_errors','Off');
    include_once("../../../vendor/autoload.php");
    
    use App\BITM\SEIP107897\Birthday\Birthday;
    
    $obj = new Birthday();
    $bdays= $obj->index() 
    
        
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Birthday</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:60%;
            }
        </style>
    </head>
    <body>
        <h1>Birthday</h1>
        <div><span>Search / Filter </span> <span id="utility">Download as PDF | XL  <a href="create.php">Add New</a>
                <a href="../../../index.html">Home</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        
        </div>
   
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>ID</th>
                    <th>Name </th>
                     <th>Birthday </th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <?php
               
               $slno =1;
               foreach($bdays as $shahriar){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>
                    <td><?php echo $shahriar->id;?></td>
                    <td><a href="#"><?php echo $shahriar->title; ?></a></td>
                    <td><?php echo $shahriar-> day;?></td>
                    <td>View | Edit | Delete | Trash/Recover | Email to Friend </td>
                </tr>
            <?php
           $slno++;
             }
            ?>
            </tbody>
        </table>
        
        <div><span> prev  1 | 2 | 3 next </span></div>
    </body>
</html>
