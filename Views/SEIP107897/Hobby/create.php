<!DOCTYPE html>
<html>
    <head>
        <title>Bitm Atomic project</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css" type="text/css">
    </head>
    <body>
        
         <!-- =============== header-section =============== -->
        
         
         
         <section class="header_part">
            <div class="container">
                <div class="row">
                    <div>
                    <div class="col-md-12">
                        <legend><h2 class="header_text text-success">Bitm Atomic project</h2></legend>
                    </div>
                    </div>
                    <div>
                        <div class="col-md-12">  					
                             <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-envelope"></span>Shahriarseu@gmail.com</a>
                            </p>
                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-hand-right"></span> SEIP-107897</a>
                            </p>
                            <p class="navbar-text pull-right">
                                <a href="#">Md.Shahriar Hossain</a>
                            </p>
                            <img src="../../../shahriar.jpg" class="img-circle pull-right" alt="Cinque Terre" width="100" height="100">
                        
                        </div>
                    </div>
            </div>
            </div>
        </section>
         
         
          <!-- =============== navbar-section =============== -->
        
          <section class="table_section">
              <div class="container">
                  <div class="row col-md-10 col-md-offset-1 custyle">
                      <div class="table_nav">
                          <nav class="navbar navbar-default" role="navigation">
                              <div class="collapse navbar-collapse navbar-ex1-collapse">
                                  <ul class="nav navbar-nav">
                                      <li><a href="#">HOME</a></li>
                                    <li><a href="">VIEW</a></li>
                                    <li><a href="#">ADD BOOK</a></li>
                                  </ul>
                                  
                                   <ul class="nav navbar-nav navbar-right"> 
                                    <form class="navbar-form navbar-left" role="search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Search">
                                        </div>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </form>

                                    <li><a href="#">Download PDF</a></li>

                                </ul>
                              </div>    
                              
                          </nav>
                          
                      </div>
                      
                      
               
        
  <!-- =============== body-section =============== -->
      
                     </div>  
                </div>
             </section>
  
      <script src="../../../Resource/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>  
      <script src="../../../Resource/bootstrap/js/ajax_jquery.js" type="text/javascript"></script>  
    </body>
</html>
